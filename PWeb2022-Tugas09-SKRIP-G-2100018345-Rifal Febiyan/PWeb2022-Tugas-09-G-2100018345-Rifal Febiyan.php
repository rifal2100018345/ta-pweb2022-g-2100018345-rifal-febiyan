<?php 
// Listing Program 9.1
echo "Listing 9.1"; 
echo "<br><br>";
$gaji =1000000;
$pajak =0.1;
$thp = $gaji-($gaji*$pajak);
echo "Gaji sebelum pajak = Rp. $gaji <br>";
echo "Gaji yang dibawa pulang = Rp. $thp ";
echo "<br><br>";

// Listing Program 9.1
echo "Listing 9.2";
echo "<br><br>";
$a = 5;
$b = 4;
echo "$a == $b : ".($a == $b);
echo "<br> $a != $b : ".($a != $b);
echo "<br> $a > $b : ".($a > $b);
echo "<br> $a < $b : ".($a < $b);
echo "<br> ($a == $b) && ($a > $b) : ".(($a != $b) && ($a > $b));
echo "<br> ($a == $b) || ($a > $b) : ".(($a != $b) || ($a > $b));
echo "<br><br>";

// MODIFIKASI
echo "Modifikasi";
echo "<br><br>";
$Y = 10;
$X = 5;
echo "Diketahui Y=10 dan X=5";
echo "<br>";
echo "Hasil dari berbagai Operasi Aritmatika";
echo "<br>";
$jumlah = $Y+$X;
echo "Y + X : $jumlah ";
echo "<br>";
$kurang = $Y-$X;
echo "Y - X : $kurang ";
echo "<br>";
$kali = $Y*$X;
echo "Y * X : $kali ";
echo "<br>";
$bagi = $Y/$X;
echo "Y / X : $bagi ";
?>